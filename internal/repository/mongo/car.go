package mongo

import (
	"ATOM/atom/internal/core"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

type CarRepository struct {
	collection *mongo.Collection
}

func NewCarRepository(collection *mongo.Collection) *CarRepository {
	return &CarRepository{collection: collection}
}

func (repository *CarRepository) Save(ctx context.Context, car *core.Car) (*core.Car, error) {

	result, err := repository.collection.InsertOne(ctx, car)

	if err != nil {
		return nil, err
	}
	car.ID = result.InsertedID.(primitive.ObjectID)

	return car, nil
}

func (repository *CarRepository) GetAll(ctx context.Context) ([]*core.Car, error) {
	cursor, err := repository.collection.Find(ctx, bson.D{})

	if err != nil {
		return nil, err
	}

	cars := make([]*core.Car, 0)

	err = cursor.All(ctx, &cars)

	if err != nil {
		return nil, err
	}

	return cars, nil
}

func (repository *CarRepository) GetByLicensePlate(ctx context.Context, licensePlate string) (*core.Car, error) {
	ctxTimeout, cancel := context.WithTimeout(ctx, time.Second*5)

	defer cancel()

	userChannel := make(chan *core.Car, 0)
	var err error

	go func() {
		err = repository.retrieveCar(ctx, licensePlate, userChannel)
	}()

	if err != nil {
		return nil, err
	}

	var user *core.Car

	select {
	case <-ctxTimeout.Done():
		break
	case user = <-userChannel:
	}

	return user, nil
}

func (repository *CarRepository) retrieveCar(ctx context.Context, licensePlate string, channel chan<- *core.Car) (err error) {

	car := &core.Car{}
	err = repository.collection.FindOne(ctx, bson.M{"licensePlate": licensePlate}).Decode(car)

	if err != nil {
		return err
	}

	channel <- car
	return nil
}

func (repository *CarRepository) Delete(ctx context.Context, licensePlate string) error {
	filter := bson.M{
		"licensePlate": licensePlate,
	}
	_, err := repository.collection.DeleteMany(ctx, filter)
	if err != nil {
		return err
	}
	return nil
}

func (repository *CarRepository) UpdateByLicense(ctx context.Context,
	licensePlate string, car *core.Car) (*core.Car, error) {

	filter := bson.M{
		"licensePlate": licensePlate,
	}

	update := bson.M{
		"$set": bson.M{
			"model":      car.Model,
			"mileage":    car.Mileage,
			"usingPrice": car.UsingPrice,
		},
	}

	_, err := repository.collection.UpdateOne(
		ctx,
		filter,
		update,
	)
	if err != nil {
		return nil, err
	}
	return car, nil
}
