package mongo

import (
	"ATOM/atom/internal/core"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type MileageRepository struct {
	collection *mongo.Collection
}

func NewMileageRepository(collection *mongo.Collection) *MileageRepository {
	return &MileageRepository{collection: collection}
}

func (repository *MileageRepository) Save(ctx context.Context,
	mileage *core.Mileage) (*core.Mileage, error) {

	result, err := repository.collection.InsertOne(ctx, mileage)

	if err != nil {
		return nil, err
	}
	mileage.ID = result.InsertedID.(primitive.ObjectID)

	return mileage, nil
}

func (repository *MileageRepository) GetMileagesByLicense(ctx context.Context,
	license string) ([]*core.Mileage, error) {
	cursor, err := repository.collection.Find(ctx, bson.M{"licensePlate": license})

	if err != nil {
		return nil, err
	}

	mileages := make([]*core.Mileage, 0)
	err = cursor.All(ctx, &mileages)

	if err != nil {
		return nil, err
	}

	return mileages, nil
}
