package core

import (
	"ATOM/atom/internal/errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"reflect"
	"strconv"
)

type Car struct {
	ID              primitive.ObjectID `bson:"_id,omitempty"`
	LicensePlate    string             `bson:"licensePlate"`
	Model           string             `bson:"model,omitempty"`
	ManufactureYear int                `bson:"manufactureYear,omitempty"`
	Mileage         int                `bson:"mileage"`
	UsingPrice      float32            `bson:"usingPrice"`
}

func (car *Car) ToString() string {
	return fmt.Sprintf(
		"%v %v %v %v %v",
		car.LicensePlate,
		car.Model,
		car.ManufactureYear,
		car.Mileage,
		car.UsingPrice,
	)
}

func GetCarPattern() string {
	return "Регистрационный номер, Модель, Год выпуска, Текущий пробег, Цена использования(Рублей за 1 км)"
}

func NewCar(fields []string) (*Car, error) {
	present := reflect.TypeOf(Car{}).NumField() - 1
	if len(fields) == present {
		year, err := strconv.Atoi(fields[2])
		if err != nil {
			return nil, &errors.ConvertError{}
		}
		mileage, err := strconv.Atoi(fields[3])
		if err != nil {
			return nil, &errors.ConvertError{}
		}
		price, err := strconv.ParseFloat(fields[4], 32)
		if err != nil {
			return nil, &errors.ConvertError{}
		}
		return &Car{
			LicensePlate:    fields[0],
			Model:           fields[1],
			ManufactureYear: year,
			Mileage:         mileage,
			UsingPrice:      float32(price),
		}, nil
	} else {
		return nil, &errors.WrongNumberOfAttrError{
			Required: present,
			Present:  len(fields),
		}
	}
}
