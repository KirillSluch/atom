package dto

type UpdateCar struct {
	Mileage int
	Model   string
}
