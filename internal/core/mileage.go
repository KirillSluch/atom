package core

import (
	"fmt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Mileage struct {
	ID           primitive.ObjectID `bson:"_id,omitempty"`
	LicensePlate string             `bson:"licensePlate"`
	DateTime     time.Time          `bson:"dateTime"`
	ThisMileage  int                `bson:"thisMileage"`
}

func (mileage *Mileage) ToString() string {
	return fmt.Sprintf(
		"%v | %v | %v",
		mileage.LicensePlate,
		mileage.ThisMileage,
		mileage.DateTime,
	)
}
