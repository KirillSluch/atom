package config

import (
	"context"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoConfig struct {
	URI      string
	Username string
	Password string
	DB       string
}

func SetupMongoDataBase(ctx context.Context, cancel context.CancelFunc) (*mongo.Database, error) {
	config := &MongoConfig{}

	err := viper.UnmarshalKey("mongo.database", config)

	if err != nil {
		return nil, err
	}
	clientCredential := options.Credential{
		Username: config.Username,
		Password: config.Password,
	}
	clientOptions := options.Client().ApplyURI(config.URI).SetAuth(clientCredential)
	client, err := mongo.Connect(ctx, clientOptions)

	if err != nil {
		return nil, err
	}

	err = client.Ping(context.Background(), nil)

	if err != nil {
		return nil, err
	}

	//defer cancel()

	return client.Database(config.DB), nil
}
