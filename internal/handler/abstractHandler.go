package handler

import (
	"ATOM/atom/internal/core"
	"ATOM/atom/internal/service"
	"context"
)

type CommandHandler struct {
	command     string
	description string
	function    func(carService CarService)
}

func NewHandler(command, description string,
	function func(carService CarService)) *CommandHandler {
	return &CommandHandler{
		command:     command,
		description: description,
		function:    function,
	}
}

type CarService interface {
	GetAll(ctx context.Context) ([]*core.Car, error)
	GetByLicensePlate(ctx context.Context, licensePlate string) (*core.Car, error)
	CreateCar(ctx context.Context, car *core.Car) (*core.Car, error)
	DeleteCar(ctx context.Context, licensePlate string) error
	CheckCarInDB(ctx context.Context, licensePlate string) (bool, error)
	IncreaseCarMileage(ctx context.Context,
		licensePlate string, mileage int) (*core.Car, error)
	ChangeCarModel(ctx context.Context,
		licensePlate string, newModel string) (*core.Car, error)
	ChangeCarUsingPrice(ctx context.Context,
		licensePlate string, price float32) (*core.Car, error)
	CalculatePriceOfKM(ctx context.Context,
		licensePlate string, km int) (float32, error)
	GetMileages(ctx context.Context, licensePlate string) ([]*core.Mileage, error)
}

func (handler *CommandHandler) GetCommand() string {
	return handler.command
}

func (handler *CommandHandler) GetDescription() string {
	return handler.description
}

func (handler *CommandHandler) Handle(service *service.CarService) {
	handler.function(service)
}
