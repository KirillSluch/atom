package impl

import (
	"ATOM/atom/internal/handler"
	"context"
	"fmt"
)

func HandleGetAllCars(service handler.CarService) {
	cars, err := service.GetAll(context.Background())
	if err != nil {
		fmt.Println("\033[31m", "Проблема с базой данных", "\033[0m")
	}

	if cars == nil {
		fmt.Println("Автомобилей в базе данных нет")
	} else {
		for _, car := range cars {
			fmt.Println("\033[33m", car.ToString(), "\033[0m")
		}
	}
}
