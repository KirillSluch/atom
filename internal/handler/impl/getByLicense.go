package impl

import (
	"ATOM/atom/internal/handler"
	"ATOM/atom/internal/ui"
	"bufio"
	"context"
	"fmt"
	"os"
)

func HandleGetByLicense(service handler.CarService) {
	reader := bufio.NewReader(os.Stdin)
	handled := false
	var command string
	for !handled && command != "0" {
		PrintGetByLicenseRules()
		command = ui.GetClearCommand(reader)
		if command == "0" {
			break
		}
		handled = handleSearchByLicense(service, command)
	}
}

func PrintGetByLicenseRules() {
	fmt.Println("Введите регистрационный номер автомобиля:")
}

func handleSearchByLicense(service handler.CarService, license string) bool {
	ui.PrintBlue("Ищем информацию в базе...")
	car, err := service.GetByLicensePlate(context.Background(), license)
	if err != nil {
		fmt.Println("Не удалось проверить автомобиль")
	} else {
		if car == nil {
			fmt.Println("Автомобиля с таким номером нет в базе")
		} else {
			fmt.Println(car.ToString())
			return true
		}
	}
	return false
}
