package impl

import (
	"ATOM/atom/internal/handler"
	"ATOM/atom/internal/ui"
	"bufio"
	"context"
	"os"
)

func HandleGetMileages(service handler.CarService) {
	reader := bufio.NewReader(os.Stdin)
	handled := false
	var command string
	for !handled && command != "0" {
		PrintGetByLicenseRules()
		command = ui.GetClearCommand(reader)
		if command == "0" {
			break
		}
		handled = handleSearchMileagesByLicense(service, command)
	}
}

func handleSearchMileagesByLicense(service handler.CarService, license string) bool {
	ui.PrintBlue("Ищем информацию в базе...")
	mileages, err := service.GetMileages(context.Background(), license)
	if err != nil {
		ui.PrintRed("Не удалось проверить автомобиль")
	} else {
		if len(mileages) == 0 {
			ui.PrintRed("Записей пробога для этого автомобиля не было обнаружено")
		} else {
			ui.PrintYellow("Номер, Пробег в день записи, Время записи пробега")
			for _, mileage := range mileages {
				ui.PrintGreen(mileage.ToString())
			}
			return true
		}
	}
	return false
}
