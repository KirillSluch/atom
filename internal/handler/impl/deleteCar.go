package impl

import (
	"ATOM/atom/internal/handler"
	"ATOM/atom/internal/ui"
	"bufio"
	"context"
	"fmt"
	"os"
)

func HandleCarDelete(service handler.CarService) {
	reader := bufio.NewReader(os.Stdin)
	handled := false
	var command string
	for !handled && command != "0" {
		PrintDeleteCarRules()
		command = ui.GetClearCommand(reader)
		if command == "0" {
			break
		}
		handled = handleDeleteByLicense(service, command)
	}
}

func handleDeleteByLicense(service handler.CarService, license string) bool {
	err := service.DeleteCar(context.Background(), license)
	if err != nil {
		fmt.Println("\033[31m", "Невозможно удалить автомобиль из базы", "\033[0m")
	} else {
		fmt.Println("\033[32m", "Автомобиль успешно удалён", "\033[0m")
		return true
	}
	return false
}

func PrintDeleteCarRules() {
	fmt.Println("Введите номер авомобиля")
}
