package impl

import (
	"ATOM/atom/internal/core"
	"ATOM/atom/internal/handler"
	"ATOM/atom/internal/ui"
	"bufio"
	"context"
	"fmt"
	"os"
	"strings"
)

func HandleAddCar(service handler.CarService) {
	reader := bufio.NewReader(os.Stdin)
	handled := false
	var command string
	for !handled && command != "0" {
		PrintSaveRules()
		command = ui.GetClearCommand(reader)
		if command == "0" {
			break
		}
		fields := strings.Split(command, ", ")
		car, err := core.NewCar(fields)
		if err != nil {
			PrintTryAgain(err)
		} else {
			fmt.Println(ui.ColorBlue, "Сохраняем автомобиль...", ui.ColorReset)
			createCar, err := service.CreateCar(context.Background(), car)
			ProcessAfterService(err, createCar, handled)
		}
	}
}

func ProcessAfterService(err error, createCar *core.Car, handled bool) {
	if err != nil {
		fmt.Println("Не удалось сохранить автомобиль")
	} else {
		fmt.Println("Автомобиль с номером " +
			createCar.LicensePlate +
			" сохранен")
		handled = true
	}
}

func PrintTryAgain(err error) {
	fmt.Println(err.Error())
	fmt.Println("Повторите попытку")
}

func PrintSaveRules() {
	fmt.Println("Введите данные в следующем формате:\n" +
		core.GetCarPattern())
}
