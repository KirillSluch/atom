package impl

import (
	"ATOM/atom/internal/handler"
	"ATOM/atom/internal/ui"
	"bufio"
	"context"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func HandleCarUpdate(service handler.CarService) {
	reader := bufio.NewReader(os.Stdin)
	handled := false
	var command string
	for !handled {
		PrintUpdateRules()
		command = ui.GetClearCommand(reader)
		if command == "0" {
			break
		}
		switch command {
		case "Model":
			handled = updateModel(service)
		case "UsingPrice":
			handled = updateUsingPrice(service)
		}
	}
}

func updateUsingPrice(service handler.CarService) bool {
	reader := bufio.NewReader(os.Stdin)
	var command string
	for {
		PrintUpdateUsingPriceRules()
		command = ui.GetClearCommand(reader)
		if command == "0" {
			break
		}

		fields := strings.Split(command, ", ")
		if len(fields) != 2 {
			ui.PrintRed("Некорректный ввод")
		} else {
			price, err := strconv.ParseFloat(fields[1], 32)
			if err != nil {
				ui.PrintRed("Проверьте правильность ввода")
				continue
			}
			_, err = service.ChangeCarUsingPrice(context.Background(), fields[0], float32(price))
			if err != nil {
				ui.PrintRed("Не удалось обновить цену использования")
				continue
			}
			ui.PrintGreen("Данные успешно обновлены")
			return true
		}
	}
	return false
}

func PrintUpdateUsingPriceRules() {
	fmt.Println("Введите данные в следующем формате:")
	ui.PrintYellow("Номер автомобиля, Цена использования")
}

func updateModel(service handler.CarService) bool {
	reader := bufio.NewReader(os.Stdin)
	var command string
	for {
		PrintUpdateModelRules()
		command = ui.GetClearCommand(reader)
		if command == "0" {
			break
		}
		fields := strings.Split(command, ", ")
		if len(fields) != 2 {
			ui.PrintRed("Некорректный ввод")
		} else {
			_, err := service.ChangeCarModel(context.Background(), fields[0], fields[1])
			if err != nil {
				ui.PrintRed("Не удалось обновить модель")
			} else {
				ui.PrintGreen("Модель успешно обновлена")
				return true
			}
		}
	}
	return false
}

func PrintUpdateModelRules() {
	fmt.Println("Введите данные в следующем формате:")
	ui.PrintYellow("Номер автомобиля, Название модели")
}

func PrintUpdateRules() {
	fmt.Println("Выберите, какие данные об автомобиле вы хотите изменить:")
	ui.PrintYellow("UsingPrice - цена использования")
	ui.PrintYellow("Model - название модели")
}
