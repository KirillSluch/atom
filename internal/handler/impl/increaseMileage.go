package impl

import (
	"ATOM/atom/internal/handler"
	"ATOM/atom/internal/ui"
	"bufio"
	"context"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func HandleIncreaseMileage(service handler.CarService) {
	reader := bufio.NewReader(os.Stdin)
	handled := false
	var command string
	for !handled && command != "0" {
		PrintIncreaseMileageRules()
		command = ui.GetClearCommand(reader)
		if command == "0" {
			break
		}
		license, mileage, err := parseLicenseAndMileage(command)
		if err != nil {
			ui.PrintRed("Проверьте правильность ввода")
		} else {
			handled = increaseMileage(service, license, mileage)
		}
	}
}

func parseLicenseAndMileage(command string) (string, int, error) {
	fields := strings.Split(command, ", ")
	mileage, err := strconv.Atoi(fields[1])
	if err != nil {
		return "", 0, err
	}
	return fields[0], mileage, nil
}

func PrintIncreaseMileageRules() {
	fmt.Println("Введите данные в следующем формате:")
	ui.PrintYellow("Номер автомобиля, Количество км на которое надо увеличить пробег")
}

func increaseMileage(service handler.CarService, license string, mileage int) bool {
	car, err := service.IncreaseCarMileage(context.Background(), license, mileage)

	if err != nil {
		ui.PrintRed("Не удалось увеличить пробег")
		return false
	}
	ui.PrintGreen("Пробег успешно увеличен")
	ui.PrintYellow(fmt.Sprintf("Теперь пробег авто составляет %v км", car.Mileage))
	return true

}
