package impl

import (
	"ATOM/atom/internal/handler"
	"ATOM/atom/internal/ui"
	"bufio"
	"context"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func HandleCarUsing(service handler.CarService) {
	reader := bufio.NewReader(os.Stdin)
	handled := false
	var command string
	for !handled && command != "0" {
		PrintCarUsingRules()
		command = ui.GetClearCommand(reader)
		if command == "0" {
			break
		}
		handled = handleCarUsingPrice(service, command)
	}
}

func PrintCarUsingRules() {
	fmt.Println("Введите данные в следующем формате:")
	ui.PrintYellow("Номер автомобиля, Количество км пройденное автомобилем")
}

func handleCarUsingPrice(service handler.CarService, command string) bool {
	fields := strings.Split(command, ", ")
	km, err := strconv.Atoi(fields[1])
	if err != nil {
		ui.PrintRed("Проверьте правильность введённых данных")
		return false
	}

	price, err := service.CalculatePriceOfKM(context.Background(), fields[0], km)

	if err != nil {
		ui.PrintRed("Не удалось посчитать стоимость использования")
		return false
	}

	ui.PrintGreen(fmt.Sprintf("Стоимость использования на %v км проега составила %v рублей",
		km, price))
	return true
}
