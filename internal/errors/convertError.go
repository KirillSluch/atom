package errors

type ConvertError struct {
}

func (e *ConvertError) Error() string {
	return "Проверьте формат вводимых данных"
}
