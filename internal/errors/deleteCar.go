package errors

import "fmt"

type DeleteCarError struct {
	LicensePlate string
}

func (e *DeleteCarError) Error() string {
	return fmt.Sprintf("Car with license plate <%v> does not exists", e.LicensePlate)
}
