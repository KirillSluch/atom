package errors

import "fmt"

type SaveCarError struct {
	LicensePlate string
}

func (e *SaveCarError) Error() string {
	return fmt.Sprintf("Car with license plate <%v> already exists", e.LicensePlate)
}
