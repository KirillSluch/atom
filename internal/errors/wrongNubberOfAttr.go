package errors

import "fmt"

type WrongNumberOfAttrError struct {
	Required int
	Present  int
}

func (e *WrongNumberOfAttrError) Error() string {
	return fmt.Sprintf("Вместо %v полей есть %v", e.Required, e.Present)
}
