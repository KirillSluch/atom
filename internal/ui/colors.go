package ui

import "fmt"

const (
	ColorReset = "\033[0m"

	ColorRed    = "\033[31m"
	ColorGreen  = "\033[32m"
	ColorYellow = "\033[33m"
	ColorBlue   = "\033[34m"
	ColorPurple = "\033[35m"
	ColorCyan   = "\033[36m"
	ColorWhite  = "\033[37m"
)

func PrintGreen(text string) {
	fmt.Println(ColorGreen, text, ColorReset)
}

func PrintRed(text string) {
	fmt.Println(ColorRed, text, ColorReset)
}

func PrintBlue(text string) {
	fmt.Println(ColorBlue, text, ColorReset)
}

func PrintYellow(text string) {
	fmt.Println(ColorYellow, text, ColorReset)
}
