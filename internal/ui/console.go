package ui

import (
	"ATOM/atom/internal/service"
	"bufio"
	"fmt"
	"os"
	"strings"
)

type Handler interface {
	GetCommand() string
	GetDescription() string
	Handle(service *service.CarService)
}

type Console struct {
	handlers []Handler
	service  *service.CarService
	menuText string
}

func (console *Console) RegisterHandler(handler Handler) {
	console.handlers = append(console.handlers, handler)
	console.menuText += fmt.Sprintf("%v - %v\n", handler.GetCommand(), handler.GetDescription())
}

func NewConsole(service *service.CarService) *Console {
	return &Console{
		handlers: make([]Handler, 0),
		service:  service,
		menuText: "0 - Завершить работу программы\n",
	}
}

func (console *Console) Start() {
	reader := bufio.NewReader(os.Stdin)
	console.PrintMenu()
	command := GetClearCommand(reader)
	var handled bool
	for command != "0" {
		handled = false
		for _, handler := range console.handlers {
			if (handler).GetCommand() == command {
				(handler).Handle(console.service)
				handled = true
			}
		}
		if !handled {
			fmt.Println("Команда не распознана, повторите попытку...")
		}
		console.PrintMenu()
		command = GetClearCommand(reader)
	}

}

func (console *Console) PrintMenu() {
	fmt.Print(console.menuText)
}

func GetClearCommand(reader *bufio.Reader) string {
	command, _ := reader.ReadString('\n')
	command = strings.TrimRight(command, "\r\n")
	return command
}
