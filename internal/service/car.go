package service

import (
	"ATOM/atom/internal/core"
	"ATOM/atom/internal/errors"
	"context"
	"time"
)

type CarRepository interface {
	GetAll(ctx context.Context) ([]*core.Car, error)
	GetByLicensePlate(ctx context.Context, licensePlate string) (*core.Car, error)
	Save(ctx context.Context, user *core.Car) (*core.Car, error)
	Delete(ctx context.Context, licensePlate string) error
	UpdateByLicense(ctx context.Context,
		licensePlate string, car *core.Car) (*core.Car, error)
}

type MileageRepository interface {
	Save(ctx context.Context, mileage *core.Mileage) (*core.Mileage, error)
	GetMileagesByLicense(ctx context.Context, license string) ([]*core.Mileage, error)
}

type CarService struct {
	carRepository     CarRepository
	mileageRepository MileageRepository
}

func NewCarService(carRepository CarRepository, mileageRepository MileageRepository) *CarService {
	return &CarService{
		carRepository:     carRepository,
		mileageRepository: mileageRepository,
	}
}

func (service *CarService) GetAll(ctx context.Context) ([]*core.Car, error) {
	return service.carRepository.GetAll(ctx)
}

func (service *CarService) GetByLicensePlate(ctx context.Context, licensePlate string) (*core.Car, error) {
	return service.carRepository.GetByLicensePlate(ctx, licensePlate)
}

func (service *CarService) CreateCar(ctx context.Context, car *core.Car) (*core.Car, error) {
	ch, err := service.CheckCarInDB(ctx, car.LicensePlate)
	if ch || err != nil {
		return nil, &errors.SaveCarError{
			LicensePlate: car.LicensePlate,
		}
	}

	return service.carRepository.Save(ctx, car)
}

func (service *CarService) DeleteCar(ctx context.Context, licensePlate string) error {
	ch, err := service.CheckCarInDB(ctx, licensePlate)
	if !ch || err != nil {
		return &errors.DeleteCarError{
			LicensePlate: licensePlate,
		}
	}
	return service.carRepository.Delete(ctx, licensePlate)
}

func (service *CarService) CheckCarInDB(ctx context.Context, licensePlate string) (bool, error) {
	checkCar, err := service.carRepository.GetByLicensePlate(ctx, licensePlate)
	if err != nil {
		return false, err
	}
	return checkCar != nil, nil
}

func (service *CarService) IncreaseCarMileage(ctx context.Context,
	licensePlate string, mileage int) (*core.Car, error) {
	storageCar, err := service.GetByLicensePlate(ctx, licensePlate)

	if err != nil {
		return nil, err
	}

	err = service.saveIncreaseMileage(ctx, licensePlate, mileage)

	if err != nil {
		return nil, err
	}

	storageCar.Mileage += mileage

	return service.carRepository.UpdateByLicense(ctx, licensePlate, storageCar)
}

func (service *CarService) saveIncreaseMileage(ctx context.Context,
	licensePlate string, mileage int) error {
	mileageInst := &core.Mileage{
		LicensePlate: licensePlate,
		DateTime:     time.Now(),
		ThisMileage:  mileage,
	}
	_, err := service.mileageRepository.Save(ctx, mileageInst)
	if err != nil {
		return err
	}
	return nil
}

func (service *CarService) ChangeCarModel(ctx context.Context,
	licensePlate string, newModel string) (*core.Car, error) {
	storageCar, err := service.GetByLicensePlate(ctx, licensePlate)

	if err != nil {
		return nil, err
	}

	storageCar.Model = newModel

	return service.carRepository.UpdateByLicense(ctx, licensePlate, storageCar)
}

func (service *CarService) ChangeCarUsingPrice(ctx context.Context,
	licensePlate string, price float32) (*core.Car, error) {
	storageCar, err := service.GetByLicensePlate(ctx, licensePlate)

	if err != nil {
		return nil, err
	}

	storageCar.UsingPrice = price

	return service.carRepository.UpdateByLicense(ctx, licensePlate, storageCar)
}

func (service *CarService) CalculatePriceOfKM(ctx context.Context,
	licensePlate string, km int) (float32, error) {
	car, err := service.GetByLicensePlate(ctx, licensePlate)
	if err != nil {
		return 0, err
	}

	age := time.Now().Year() - car.ManufactureYear
	koef := calculateKoef(age, car.Mileage)

	return koef * car.UsingPrice * float32(km), nil
}

func calculateKoef(age int, mileage int) float32 {
	if age == 0 {
		return 2.5
	}
	if age <= 5 {
		if mileage < 150_000 {
			return 1
		} else {
			return 1.5
		}
	} else {
		if mileage < 150_000 {
			return 1.5
		} else {
			return 2
		}
	}
}

func (service *CarService) GetMileages(ctx context.Context,
	licensePlate string) ([]*core.Mileage, error) {
	mileages, err := service.mileageRepository.GetMileagesByLicense(ctx, licensePlate)

	if err != nil {
		return nil, err
	}

	return mileages, nil
}
