package main

import (
	"ATOM/atom/internal/config"
	_ "ATOM/atom/internal/core"
	"ATOM/atom/internal/handler"
	"ATOM/atom/internal/handler/impl"
	"ATOM/atom/internal/repository/mongo"
	"ATOM/atom/internal/service"
	"ATOM/atom/internal/ui"
	"context"
	_ "fmt"
	"github.com/spf13/viper"
	mongo2 "go.mongodb.org/mongo-driver/mongo"
	"log"
	"time"
)

func main() {
	if err := SetupViper(); err != nil {
		log.Fatal(err.Error())
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)

	mongoDataBase := InitialiseDataBase(ctx, cancel)

	carRepository := mongo.NewCarRepository(mongoDataBase.Collection("cars"))
	mileageRepository := mongo.NewMileageRepository(mongoDataBase.Collection("mileages"))
	carService := service.NewCarService(carRepository, mileageRepository)

	console := ui.NewConsole(carService)

	registerHandlers(console)
	console.Start()
}

func registerHandlers(console *ui.Console) {
	console.RegisterHandler(handler.NewHandler("1",
		"Получить информацию об автомобиле", impl.HandleGetByLicense))
	console.RegisterHandler(handler.NewHandler("2",
		"Добавить новый автомобиль", impl.HandleAddCar))
	console.RegisterHandler(handler.NewHandler("3",
		"Получить информацию о всех автомобилях", impl.HandleGetAllCars))
	console.RegisterHandler(handler.NewHandler("4",
		"Удалить автомобиль", impl.HandleCarDelete))
	console.RegisterHandler(handler.NewHandler("5",
		"Увеличить пробег по номерному знаку", impl.HandleIncreaseMileage))
	console.RegisterHandler(handler.NewHandler("6",
		"Обновить данные автомобиля", impl.HandleCarUpdate))
	console.RegisterHandler(handler.NewHandler("7",
		"Посчитать стоимость пробега", impl.HandleCarUsing))
	console.RegisterHandler(handler.NewHandler("8",
		"Отследить пробег", impl.HandleGetMileages))
}

func InitialiseDataBase(ctx context.Context, cancel context.CancelFunc) *mongo2.Database {
	mongoDataBase, err := config.SetupMongoDataBase(ctx, cancel)
	if err != nil {
		log.Fatal(err.Error())
	}

	err = mongoDataBase.CreateCollection(ctx, "cars")
	if err != nil {
		if _, ok := err.(mongo2.CommandError); !ok {
			log.Println("Creating collection cars")
			log.Fatal(err.Error())
		}
	}

	err = mongoDataBase.CreateCollection(ctx, "mileages")
	if err != nil {
		if _, ok := err.(mongo2.CommandError); !ok {
			log.Println("Creating collection mileages")
			log.Fatal(err.Error())
		}
	}
	return mongoDataBase
}

func SetupViper() error {
	viper.AddConfigPath("atom/configs")
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	return nil
}
